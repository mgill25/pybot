#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Plugin Management

Plugins are used to extend PyBot's functionality.

Each plugin should have the following basic attributes:
    1. Name
    2. Description (used for !help <plugin_name>)
    3. Category
    4. An action() method, that will override the base class' method.
"""
# TODO: Abstract over the regex-handling. Plugin writers shouldn't have to deal
# with that stuff.

__plugins__ = []

class Plugin(object):

    def __init__(self, name, category, description):

        self.name = name                # The name will also be the plugin command.
        self.category = category
        self.description = description

    def action(self, *options):
        pass
