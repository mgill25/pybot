#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IRC related stuff.

import socket
import asynchat
import logging
import re

# Logging
logger = logging.getLogger("PYBOT")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
fmt = logging.Formatter("%(asctime)s - %(name)s[%(levelname)s]- %(message)s")
ch.setFormatter(fmt)
logger.addHandler(ch)

# Global Patterns
PING_PAT = re.compile(r':?PING(.*)')
JOIN_PAT = re.compile(r':(\w+)!(\w+@[\w\d\-.]+)\sJOIN\s:#(\w+)')

class REMatch(object):
    """
    Easily test against multiple regex patterns.
    Ref: http://stackoverflow.com/questions/2554185/match-groups-in-python
    """
    def __init__(self, string):
        self.string = string

    def match(self, regex):
        self.re_match = re.match(regex, self.string)
        return bool(self.re_match)

    def group(self, i):
        return self.re_match.group(i)

    def groups(self):
        return self.re_match.groups()


class Client(object):

    def __init__(self, host, port):

        self.host = host
        self.port = port
        self.s_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def _connect(self):
        """Establish a Socket connection to a given network address"""
        try:
            serv_addr = (self.host, self.port)
            self.s_client.connect(serv_addr)
            logger.info('Connected to %s:%d' % (serv_addr))
        except Exception as e:
            logger.error("Connecting to host %s : %s" % (self.host, str(e)))

    def listen(self):
        while True:
            chunk = self.s_client.recv(500)
            logger.info("RESP: %s" % chunk)
            self.perform_action(chunk)

    def setup_connection(self):
        """Setup an initial connection to an IRC network"""
        self._connect()
        self.send_raw_data("USER PyBot 8 * :Py Bot")
        self.send_raw_data("NICK PyBot")
        self.send_raw_data("JOIN #test")
        self.listen()

    def send_raw_data(self, msg):
        """Send msg over the s_client socket"""
        try:
            sent = self.s_client.send(msg + "\n")
            if sent == 0:
                raise RuntimeError("Socket Connection Broken")
        except Exception as e:
            logger.error("Could not send data: %s" % e)

    def send_privmsg(self, receiver, recv_type, msg):
        """recv_type can be channel or user"""
        if recv_type == "chan" or recv_type == "channel":
            self.send_raw_data("PRIVMSG #%s :%s" % (receiver, msg))
        elif recv_type == "user":
            self.send_raw_data("PRIVMSG %s :%s" % (receiver, msg))

    def perform_action(self, chunk):
        """Perform some action based on the command fed to the bot"""
        _m = REMatch(chunk)
        if _m.match(PING_PAT):
            self.send_raw_data(_m.group(1).strip())
        elif _m.match(JOIN_PAT):
            self.send_privmsg(_m.group(3), "channel", "Greetings, %s!" % _m.group(1))

if __name__ == '__main__':
    HOST = 'irc.darklordpotter.net'
    PORT = 6667
    pybot = Client(HOST, PORT)
    pybot.setup_connection()
