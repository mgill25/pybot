# PyBot - An IRC Bot that does stuff.

## Stuff I'm planning - Hopefully, this all will make it in the bot someday.

1. Plugin Architecture.
2. Multi-threading for Bot actions.
3. Configuration Management
4. Simultaneously connect with multiple servers.
5. Runtime configuration - dynamic plugin loading etc.
6. Authentication

Inspired by rBot by Tom Gilbert.
